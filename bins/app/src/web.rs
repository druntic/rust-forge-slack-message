use actix_web::{ App, HttpServer };
use crate::application::slack_message::infrastructure::PgService;
use actix_web::web::Data;
use std::sync::Arc;

#[actix_web::main]
pub async fn start_server() -> std::io::Result<()> {
    let service = Arc::new(PgService {}); 
    HttpServer::new(move || {
        App::new()
        .app_data(Data::new(service.clone()))
        .configure(|cfg| crate::application::configure::configure(cfg))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}