use actix_web::{web, HttpResponse};
pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.route(
        "/_health",
        web::get().to(|| async { HttpResponse::Ok().body("I'm healthy".to_string()) }),
    );
    slack_message(cfg);
}
fn slack_message (c:  &mut web::ServiceConfig) {
    crate::application::slack_message::setup::routes(c);
}