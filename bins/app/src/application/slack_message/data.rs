use support::schema::slack_messages;
use serde::{Deserialize, Serialize};
use validr::*;
use diesel::{Insertable};

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]

pub struct NewSlackMessageData {
    pub name: Option<String>,
    pub channel: Option<String>,
    pub icon_emoji: Option<String>,
    pub message: Option<String>,
}


#[derive(Insertable, Debug, Clone)]
#[diesel(table_name = slack_messages)]
#[diesel(treat_none_as_null = true)]
pub struct NewSlackMessage {
    pub name: String,
    pub channel: String,
    pub icon_emoji: String,
    pub message: String,
}

impl NewSlackMessageData{

    //convert NewSlackMessageData to NewSlackMessage
    pub fn convert(self) -> NewSlackMessage {
        let name = self.name.unwrap();
        let channel = self.channel.unwrap();
        let icon_emoji = self.icon_emoji.unwrap();
        let message = self.message.unwrap();
        NewSlackMessage{
            name,
            channel,
            icon_emoji,
            message,
        }

    }
}

impl Validation for NewSlackMessageData{

    fn modifiers(&self) -> Vec<Modifier<Self>> {
        vec![]
    }

    fn rules(&self) -> Vec<Rule<Self>> {
        vec![
            rule_required!(name),
            rule_required!(channel),
            rule_required!(icon_emoji),
            rule_required!(message),
        ]
    }


}