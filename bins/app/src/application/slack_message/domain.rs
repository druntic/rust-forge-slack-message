use crate::application::slack_message::contract::{CreateMessageContract, PgServiceContract};
use async_trait::async_trait;
use crate::application::slack_message::data::NewSlackMessage;
use support::models::SlackMessage;
use error::Error;
use actix_web::http::StatusCode;

pub struct CreateMessage<A: PgServiceContract> {
    pub service: A
}

#[async_trait]
impl<A> CreateMessageContract for CreateMessage<A>
where
    A: PgServiceContract + Sync + Send,
    {
        async fn create_message(&self, message_data: NewSlackMessage ) -> Result<SlackMessage, Error> {
                
            let slack_message = self.service.create_message(message_data.clone()).await?;

            let response =self.service.send_to_slack(message_data).await?;
                // Check the status code of the response
                match response.status() {
                    StatusCode::OK => self.service.update_message(&slack_message.id).await, 
                    StatusCode::NOT_FOUND => Err(Error::NotFoundWithCause("channel not found".to_string())),
                    _ => Err(Error::InternalError("something went wrong".to_string())) 
                }
        }
    }
    
