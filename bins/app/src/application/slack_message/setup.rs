use actix_web::web;
use crate::application::slack_message::http;
use crate::application::slack_message::domain::CreateMessage;
use crate::application::slack_message::infrastructure::PgService;

pub fn routes(cfg: &mut web::ServiceConfig) {
    cfg.route(
        "/create",
        web::post().to(http::handle_create::<CreateMessage<PgService>>)
    );
    
}
