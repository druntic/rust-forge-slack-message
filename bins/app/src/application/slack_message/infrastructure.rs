use crate::application::slack_message::contract::{PgServiceContract};
use infrastructure::db;
use diesel::prelude::*;
use async_trait::async_trait;
use crate::application::slack_message::data::{NewSlackMessage};
use support::models::SlackMessage;
use error::Error;
use support::schema::slack_messages::dsl::*; 
pub struct PgService {
}
#[async_trait]
impl PgServiceContract for PgService{

    async fn create_message(&self, message_data: NewSlackMessage) -> Result<SlackMessage, Error> {
        let connection = &mut db::establish_connection();
        
    // Insert the new message into the database
        diesel::insert_into(slack_messages)
        .values(message_data)
        .get_result::<SlackMessage>(connection)
        .map_err(Error::from)
    }

    async fn send_to_slack(&self, message_data: NewSlackMessage) -> Result<reqwest::Response, reqwest::Error> {
        let body = format!(
            "{{\"channel\": \"{channel}\", \"username\": \"{name}\", \"text\": \"{message}\", \"icon_emoji\": \":{icon_emoji}:\"}}",
            message = message_data.message, name = message_data.name, 
            channel = message_data.channel, icon_emoji = message_data.icon_emoji
        );
        let slack_url = env::var("SLACK_URL").expect("SLACK_URL must be set");
        let client = reqwest::Client::new();
            client
            .post(slack_url)
            .body(body)
            .send()
            .await
    }
    

    // Update the checked column in the slack_messages table
    async fn update_message(&self, message_id: &str) -> Result<SlackMessage, Error> {
        let connection = &mut db::establish_connection();
    
        // Update the checked column to true and get the updated row as a result
        diesel::update(slack_messages.filter(id.eq(message_id)))
            .set(checked.eq(true))
            .get_result::<SlackMessage>(connection)
            .map_err(|e| error::Error::from(e))
    }
}