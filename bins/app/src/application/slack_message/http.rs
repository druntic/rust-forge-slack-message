use actix_web::*;
use crate::application::slack_message::data;
use crate::application::slack_message::contract;
use crate::application::slack_message::domain;
use crate::application::slack_message::infrastructure::PgService;
use crate::application::slack_message::contract::{CreateMessageContract};
use validr::Validation;


pub async fn handle_create<T: contract::CreateMessageContract>(data: web::Json<data::NewSlackMessageData>,
    _request: HttpRequest) -> Result<HttpResponse, Error> {
 let message_data = data.into_inner().validate()?.convert();
 let service = domain::CreateMessage {
     service: PgService {}
 };
 service.create_message(message_data.clone()).await?;
 Ok(HttpResponse::Ok().body("Message created, sent to slack and updated"))
}
