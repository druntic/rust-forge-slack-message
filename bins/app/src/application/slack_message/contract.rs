use error::Error;
use support::models::{SlackMessage};
use async_trait::async_trait;
use crate::application::slack_message::data::{ NewSlackMessage};

#[async_trait]
pub trait CreateMessageContract {
    // Define the function signature
    async fn create_message(&self, attributes: NewSlackMessage) 
    -> Result<SlackMessage, Error>;

}
    
// used for creating, updating and deleting
#[async_trait]
pub trait PgServiceContract {
    async fn create_message(&self, message_data: NewSlackMessage) -> Result<SlackMessage, Error>;
    async fn send_to_slack(
        &self, message_data: NewSlackMessage
    ) -> Result<reqwest::Response, reqwest::Error>;
    async fn update_message(&self, message_id: &str) -> Result<SlackMessage, Error>;
}
