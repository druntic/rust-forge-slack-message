pub mod web;
pub mod application;
use std::env;


fn main(){
    dotenv().ok();
    web::start_server().expect("something went wrong");
}