## Summary

Create routes to post messages on slack public channels and direct messages.
Messages are going to be sent via routes that we are going to create, and it will use reqwest crate to
communicate with Slack. Through the routes user will have option to pick slack channel, type
message, enter username and emoji. Technologies that are used are: Rust, PostgreSQL, Redis.