use diesel::result::Error as DieselError;
use validr::error::ValidationErrors;
use std::{
    error::Error as StdError, fmt,
};
use actix_web::{
    {HttpResponse, ResponseError},
};
use reqwest::Error as ReqwestError; 
#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
pub enum Error {
    // storage / file upload error
    NotFound,
    Diesel(DieselError),
    Validation(ValidationErrors),
    Reqwest(ReqwestError),
    NotFoundWithCause(String),
    InternalError(String),

}

// Allow the use of "{}" format specifier
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::NotFound => write!(f, "Not Found"),
            Error::Diesel(ref cause) => write!(f, "DB Error: {}", cause),
            Error::Validation(ref cause) => {
                write!(f, "Validation error: {}", cause)
            },
            Error::Reqwest(ref cause) => {
                write!(f, "Reqwest error: {}", cause)
            },
            Error::NotFoundWithCause(ref cause) => write!(f, "Not found: {}", cause),
            Error::InternalError(ref cause) => write!(f, "{}", cause),
        }
    }
}

impl StdError for Error {
    fn cause(&self) -> Option<&dyn StdError> {
        match *self {
            Error::NotFound => None,
            Error::Diesel(ref cause) => Some(cause),
            Error::Validation(ref cause) => Some(cause),
            Error::Reqwest(ref cause) => Some(cause),
            Error::NotFoundWithCause(ref _cause) => None,
            Error::InternalError(ref _cause) => None,
        }
    }
}

impl From<DieselError> for Error {
    fn from(cause: DieselError) -> Error {
        if cause == DieselError::NotFound {
            return Error::NotFound;
        }
        Error::Diesel(cause)
    }
}

impl From<ValidationErrors> for Error {
    fn from(cause: ValidationErrors) -> Error {
        Error::Validation(cause)
    }
}

impl From<ReqwestError> for Error { // Implement the From trait for reqwest errors
    fn from(cause: ReqwestError) -> Error {
        Error::Reqwest(cause)
    }
}


#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct ErrorBody {
    pub message: Option<String>,
    pub code: String,
    pub cause: Option<String>,
    pub payload: Option<serde_json::Value>,
}


impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {

        let mut response = match self {
            Error::NotFound => HttpResponse::NotFound(),
            Error::NotFoundWithCause(_cause) => HttpResponse::NotFound(),
            Error::InternalError(_cause) => HttpResponse::InternalServerError(),
            _ => HttpResponse::InternalServerError(),
        };

        response.json(self.into_error_body())
    }
}

impl Error {
    pub fn into_error_body(&self) -> ErrorBody {
        match *self {
            Error::Diesel(ref cause) => ErrorBody {
                message: Some("DB Error".to_string()),
                code: "db".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::NotFound => ErrorBody {
                message: Some("Entity not found".to_string()),
                code: 404.to_string(),
                cause: None,
                payload: None,
            },

            Error::Validation(ref _cause) => ErrorBody {
                message: Some("Invalid body".to_string()),
                code: 400.to_string(),
                cause: None,
                payload: None,
            },
            Error::Reqwest(ref cause) => ErrorBody {
                message: Some("Reqwest error".to_string()),
                code: "reqwest".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::NotFoundWithCause(ref cause) => ErrorBody {
                message: Some("Entity not found".to_string()),
                code: "not_found".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::InternalError(ref cause) => ErrorBody {
                message: Some("Internal server error".to_string()),
                code: "server".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            
        }
    }
}