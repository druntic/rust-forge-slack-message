use diesel::prelude::*;


#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::slack_messages)]
#[diesel(check_for_backend(diesel::pg::Pg))]
#[derive(serde::Serialize)]
pub struct SlackMessage {
    pub id: String,
    pub name: String,
    pub channel: String,
    pub icon_emoji: String,
    pub message: String,
    pub created_at: chrono::naive::NaiveDateTime,
    pub updated_at: chrono::naive::NaiveDateTime,
    pub checked: bool,
}
